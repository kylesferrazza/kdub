// @ts-check
import ts from "typescript-eslint";
import prettierConfigRecommended from "eslint-plugin-prettier/recommended";

const config = [...ts.configs.strict, prettierConfigRecommended];

export default config;
