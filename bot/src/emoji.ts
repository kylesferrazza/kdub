export const EMOJI = {
  ALPHABET: {
    A: "🇦",
    B: "🇧",
    C: "🇨",
    D: "🇩",
    E: "🇪",
    F: "🇫",
    G: "🇬",
    H: "🇭",
    I: "🇮",
    J: "🇯",
    K: "🇰",
    L: "🇱",
    M: "🇲",
    N: "🇳",
    O: "🇴",
    P: "🇵",
    Q: "🇶",
    R: "🇷",
    S: "🇸",
    T: "🇹",
    U: "🇺",
    V: "🇻",
    W: "🇼",
    X: "🇽",
    Y: "🇾",
    Z: "🇿",
  },
  DIGITS: {
    ZERO: "0️⃣",
    ONE: "1️⃣",
    TWO: "2️⃣",
    THREE: "3️⃣",
    FOUR: "4️⃣",
    FIVE: "5️⃣",
    SIX: "6️⃣",
    SEVEN: "7️⃣",
    EIGHT: "8️⃣",
    NINE: "9️⃣",
  },
  OTHERS: {
    THUMBS_UP: "👍",
    THUMBS_DOWN: "👎",
    HEART: "❤️",
    ASTONISHED: "😲",
    PLUS: "➕",
    PEOPLE_HOLDING_HANDS: "🧑‍🤝‍🧑",
    MINI_DISC: "💽",
    COOKING: "🍳",
    SHALLOW_PAN_OF_FOOD: "🥘",
    CRAB: "🦀",
    PEACE: "✌️",
    SALT: "🧂",
  },
};
