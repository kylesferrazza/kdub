import {
  Colors,
  CommandInteraction,
  EmbedBuilder,
  PermissionFlagsBits,
} from "discord.js";
import { Discord, Slash } from "discordx";
import { CommandBase } from "../commandbase";

const CHANGELOG = `
- Uh another rewrite
  - ported everything you've come to expect
- User-servicable emotes -- \`/add-emote\`, \`/emote\`, \`/remove-emote\`
- Web dashboard for management (admin-only right now)
`;

@Discord()
export class Changelog extends CommandBase {
  @Slash({
    description: "Send changelog in this channel",
    name: "changelog",
    defaultMemberPermissions: [PermissionFlagsBits.Administrator],
  })
  async changelog(interaction: CommandInteraction): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply();
    const embed = new EmbedBuilder()
      .setColor(Colors.DarkGreen)
      .setTitle("What's new?")
      .setDescription(CHANGELOG);
    await interaction.followUp({ embeds: [embed] });
  }
}
