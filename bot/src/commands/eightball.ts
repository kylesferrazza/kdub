import {
  ApplicationCommandOptionType,
  Colors,
  CommandInteraction,
  EmbedBuilder,
} from "discord.js";
import { Discord, Slash, SlashOption } from "discordx";
import { CommandBase } from "@/commandbase";
import { need } from "@/util";

const RESPONSES = [
  "It is certain.",
  "It is decidedly so.",
  "Without a doubt.",
  "Yes - definitely.",
  "You may rely on it.",
  "As I see it, yes.",
  "Most likely.",
  "Outlook good.",
  "Yes.",
  "Signs point to yes.",
  "Reply hazy, try again.",
  "Ask again later.",
  "Better not tell you now.",
  "Cannot predict now.",
  "Concentrate and ask again.",
  "Don't count on it.",
  "My reply is no.",
  "My sources say no.",
  "Outlook not so good.",
  "Very doubtful.",
];

function randIndex(l: string[]): number {
  return Math.floor(Math.random() * l.length);
}

function eightBall(): string {
  return need("8ball response", RESPONSES.at(randIndex(RESPONSES)));
}

@Discord()
export class EightBall extends CommandBase {
  @Slash({ description: "Ask the magic 8ball a question", name: "8ball" })
  async eightBall(
    @SlashOption({
      description: "the question you would like to ask the 8ball",
      type: ApplicationCommandOptionType.String,
      name: "query",
      required: true,
    })
    query: string,
    interaction: CommandInteraction,
  ): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply();
    const embed = new EmbedBuilder()
      .setColor(Colors.Blurple)
      .setTitle("8ball response")
      .addFields(
        {
          name: "Query",
          value: query,
        },
        {
          name: "Response",
          value: eightBall(),
        },
      );
    await interaction.followUp({ embeds: [embed] });
  }
}
