import { CommandInteraction } from "discord.js";
import { Discord, Slash } from "discordx";
import { CommandBase } from "../commandbase";

@Discord()
export class GM extends CommandBase {
  @Slash({ description: "Wish KDUB a good morning", name: "gm" })
  async hello(interaction: CommandInteraction): Promise<void> {
    this.logInteraction(interaction);
    await interaction.reply(`Good morning, ${interaction.user}!`);
  }
}
