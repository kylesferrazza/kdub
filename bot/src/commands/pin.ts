import {
  ApplicationCommandType,
  MessageContextMenuCommandInteraction,
} from "discord.js";
import { ContextMenu, Discord } from "discordx";
import { CommandBase } from "@/commandbase";

@Discord()
export class Pin extends CommandBase {
  @ContextMenu({
    name: "pin",
    type: ApplicationCommandType.Message,
  })
  async pin(interaction: MessageContextMenuCommandInteraction): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply({ ephemeral: true });
    if (interaction.targetMessage.pinned) {
      await interaction.followUp("Message is already pinned");
    } else {
      await interaction.targetMessage.pin();
      await interaction.followUp("Message successfully pinned");
    }
  }

  @ContextMenu({
    name: "unpin",
    type: ApplicationCommandType.Message,
  })
  async unpin(
    interaction: MessageContextMenuCommandInteraction,
  ): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply({ ephemeral: true });
    if (interaction.targetMessage.pinned) {
      await interaction.targetMessage.unpin();
      await interaction.followUp("Message successfully unpinned");
    } else {
      await interaction.followUp("Message is not pinned");
    }
  }
}
