import {
  ApplicationCommandOptionType,
  Colors,
  CommandInteraction,
  EmbedBuilder,
} from "discord.js";
import { Discord, Slash, SlashOption } from "discordx";
import { CommandBase } from "../commandbase";
import { EMOJI } from "@/emoji";
import { need } from "@/util";

const CHOICES = [
  EMOJI.ALPHABET.A,
  EMOJI.ALPHABET.B,
  EMOJI.ALPHABET.C,
  EMOJI.ALPHABET.D,
  EMOJI.ALPHABET.E,
  EMOJI.ALPHABET.F,
  EMOJI.ALPHABET.G,
  EMOJI.ALPHABET.H,
  EMOJI.ALPHABET.I,
  EMOJI.ALPHABET.J,
  EMOJI.ALPHABET.K,
  EMOJI.ALPHABET.L,
  EMOJI.ALPHABET.M,
  EMOJI.ALPHABET.N,
  EMOJI.ALPHABET.O,
  EMOJI.ALPHABET.P,
  EMOJI.ALPHABET.Q,
  EMOJI.ALPHABET.R,
  EMOJI.ALPHABET.S,
  EMOJI.ALPHABET.T,
  EMOJI.ALPHABET.U,
  EMOJI.ALPHABET.V,
  EMOJI.ALPHABET.W,
  EMOJI.ALPHABET.X,
  EMOJI.ALPHABET.Y,
  EMOJI.ALPHABET.Z,
];

function emojiForIdx(num: number): string {
  return need("emoji from list", CHOICES.at(num));
}

interface Choice {
  name: string;
  value: string;
}

function choiceParser(input: string): Choice[] {
  const options = input.split("||");
  return options.map((name, i) => ({
    name,
    value: emojiForIdx(i),
  }));
}

@Discord()
export class Poll extends CommandBase {
  @Slash({ description: "start a poll", name: "poll" })
  async poll(
    @SlashOption({
      description: "the title of your poll",
      type: ApplicationCommandOptionType.String,
      name: "title",
      required: true,
    })
    title: string,
    @SlashOption({
      description: "choices for your poll, separated by ||",
      transformer: choiceParser,
      type: ApplicationCommandOptionType.String,
      name: "choices",
      required: true,
    })
    choices: Choice[],
    interaction: CommandInteraction,
  ): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply();
    const embed = new EmbedBuilder()
      .setColor(Colors.Blurple)
      .setTitle(title)
      .setFields(choices);
    const message = await interaction.followUp({ embeds: [embed] });
    choices.forEach(async (choice) => {
      await message.react(choice.value);
    });
  }
}
