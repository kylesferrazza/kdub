import { Colors, CommandInteraction, EmbedBuilder } from "discord.js";
import { Discord, Slash } from "discordx";
import { CommandBase } from "../commandbase";

@Discord()
export class Source extends CommandBase {
  @Slash({
    description: "get the link to the bot's source code",
    name: "source",
  })
  async source(interaction: CommandInteraction): Promise<void> {
    this.logInteraction(interaction);
    const embed = new EmbedBuilder()
      .setColor(Colors.DarkGold)
      .setTitle("Source")
      .setDescription("Bot's source code on GitLab")
      .setThumbnail(
        "https://images.ctfassets.net/xz1dnu24egyd/1IRkfXmxo8VP2RAE5jiS1Q/ea2086675d87911b0ce2d34c354b3711/gitlab-logo-500.png",
      )
      .setURL("https://gitlab.com/kylesferrazza/kdub");
    await interaction.reply({ embeds: [embed] });
  }
}
