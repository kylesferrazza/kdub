import {
  ApplicationCommandOptionType,
  AutocompleteInteraction,
  Colors,
  CommandInteraction,
  EmbedBuilder,
} from "discord.js";
import { Discord, Slash, SlashOption } from "discordx";
import { CommandBase } from "../commandbase";
import prisma from "@/db";

async function autocompleteOwnedEmote(interaction: AutocompleteInteraction) {
  if (!interaction.guildId) {
    throw new Error("no guild ID!");
  }
  const emotes = await prisma.emote.findMany({
    where: {
      guild: {
        id: interaction.guildId,
      },
      owner: {
        is: {
          providerAccountId: interaction.user.id,
        },
      },
    },
  });
  await interaction.respond(
    emotes.map((e) => ({ name: e.name, value: e.name })),
  );
}

async function autocompleteEmote(interaction: AutocompleteInteraction) {
  const emotes = await prisma.emote.findMany({
    where: {
      guildId: interaction.guildId,
    },
  });
  await interaction.respond(
    emotes.map((e) => ({ name: e.name, value: e.name })),
  );
}

@Discord()
export class EmoteCmd extends CommandBase {
  @Slash({ description: "add an emote", name: "add-emote" })
  async addEmote(
    @SlashOption({
      name: "name",
      description: "the name of the emote",
      type: ApplicationCommandOptionType.String,
      required: true,
    })
    name: string,
    @SlashOption({
      name: "url",
      description: "the URL of the image to use for the emote",
      type: ApplicationCommandOptionType.String,
      required: true,
    })
    url: string,
    interaction: CommandInteraction,
  ): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply({ ephemeral: true });
    if (!interaction.guildId) {
      await interaction.followUp(
        "Non-guild emotes are currently not supported.",
      );
      return;
    }
    const created = await prisma.emote.create({
      data: {
        name,
        url,
        guild: {
          connect: {
            id: interaction.guildId,
          },
        },
        owner: {
          connectOrCreate: {
            where: {
              provider_providerAccountId: {
                provider: "discord",
                providerAccountId: interaction.user.id,
              },
            },
            create: {
              type: "oauth",
              provider: "discord",
              providerAccountId: interaction.user.id,
              user: {
                create: {
                  email: "not-yet-logged-in@localhost",
                  name:
                    interaction.user.globalName || interaction.user.displayName,
                },
              },
            },
          },
        },
      },
      select: {
        id: true,
      },
    });

    await interaction.followUp("Emote added with ID " + created.id.toString());
  }

  @Slash({ description: "trigger an emote", name: "emote" })
  async emote(
    @SlashOption({
      name: "name",
      description: "the name of the emote",
      type: ApplicationCommandOptionType.String,
      required: true,
      autocomplete: autocompleteEmote,
    })
    name: string,
    interaction: CommandInteraction,
  ): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply();
    const emote = await prisma.emote.findUnique({
      where: { name, guildId: interaction.guildId },
    });
    if (!emote) {
      await interaction.followUp("No such emote");
      return;
    }

    const embed = new EmbedBuilder()
      .setColor(Colors.Aqua)
      .setTitle(name)
      .setImage(emote.url);
    await interaction.followUp({ embeds: [embed] });
  }

  @Slash({ description: "remove an emote", name: "remove-emote" })
  async removeEmote(
    @SlashOption({
      name: "name",
      description: "the name of the emote",
      type: ApplicationCommandOptionType.String,
      required: true,
      autocomplete: autocompleteOwnedEmote,
    })
    name: string,
    interaction: CommandInteraction,
  ): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply({ ephemeral: true });
    if (!interaction.guildId) {
      await interaction.reply("Could not get Guild ID");
      return;
    }
    const deleted = await prisma.emote.delete({
      where: {
        name,
        guild: {
          id: interaction.guildId,
        },
        owner: {
          is: {
            providerAccountId: interaction.user.id,
          },
        },
      },
    });

    await interaction.followUp(
      "Emote removed with ID " + deleted.id.toString(),
    );
  }
}
