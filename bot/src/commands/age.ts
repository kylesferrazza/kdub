import {
  ApplicationCommandOptionType,
  CommandInteraction,
  User,
} from "discord.js";
import { Discord, Slash, SlashOption } from "discordx";
import { CommandBase } from "../commandbase";

function UserOrSender(
  input: { user: User } | null,
  interaction: CommandInteraction,
): User {
  return input?.user ?? interaction.user;
}

@Discord()
export class Age extends CommandBase {
  @Slash({ description: "Get a user's Discord account age", name: "age" })
  async age(
    @SlashOption({
      description: "Selected user (defaults to yourself)",
      name: "user",
      required: false,
      transformer: UserOrSender,
      type: ApplicationCommandOptionType.User,
    })
    user: User,
    interaction: CommandInteraction,
  ): Promise<void> {
    this.logInteraction(interaction);
    const age = user.createdAt;
    await interaction.reply(`${user}'s account was created at \`${age}\``);
  }
}
