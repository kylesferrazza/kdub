import {
  ApplicationCommandOptionType,
  CommandInteraction,
  Guild,
} from "discord.js";
import { Discord, Slash, SlashOption } from "discordx";
import { CommandBase } from "@/commandbase";
import { TodoistApi } from "@doist/todoist-api-typescript";
import { need } from "@/util";

const PROJECT_NAME = "Discord bots";

async function saveSuggestion(
  suggestion: string,
  guild: Guild,
  description: string,
) {
  const token = process.env.TODOIST_API_KEY;
  if (!token) {
    throw new Error("no Todoist token set");
  }
  const api = new TodoistApi(token);

  const projects = await api.getProjects();
  let botProject = projects.find((p) => p.name == PROJECT_NAME);
  if (!botProject) {
    botProject = await api.addProject({ name: PROJECT_NAME });
  }

  const sections = await api.getSections(botProject.id);
  let section = sections.find((s) => s.name.includes(guild.id));
  if (!section) {
    section = await api.addSection({
      projectId: botProject.id,
      name: `${guild.name} (${guild.id})`,
    });
  }

  await api.addTask({
    sectionId: section.id,
    content: suggestion,
    description,
  });
}

@Discord()
export class Feedback extends CommandBase {
  @Slash({ description: "suggest a new feature", name: "feedback" })
  async suggestion(
    @SlashOption({
      description: "feature you want to suggest",
      type: ApplicationCommandOptionType.String,
      name: "suggestion",
      required: true,
    })
    suggestion: string,
    interaction: CommandInteraction,
  ): Promise<void> {
    this.logInteraction(interaction);
    await interaction.deferReply({ ephemeral: true });
    const description = `
      Sender: ${interaction.user.displayName} (${interaction.user.username}).
      Date: ${interaction.createdAt.toLocaleDateString()}
      Time: ${interaction.createdAt.toLocaleTimeString()}
    `;
    await saveSuggestion(
      suggestion,
      need("guild ID", interaction.guild),
      description,
    );
    await interaction.followUp("Noted.");
  }
}
