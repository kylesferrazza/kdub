import { dirname, importx } from "@discordx/importer";
import type { Interaction, Message } from "discord.js";
import { ActivityType, IntentsBitField } from "discord.js";
import { Client } from "discordx";
import prisma from "@/db";

export const bot = new Client({
  // To use only guild command
  // botGuilds: [(client) => client.guilds.cache.map((guild) => guild.id)],
  botId: "kdub",

  // Discord intents
  intents: [
    IntentsBitField.Flags.Guilds,
    IntentsBitField.Flags.GuildMembers,
    IntentsBitField.Flags.GuildMessages,
    IntentsBitField.Flags.GuildMessageReactions,
    IntentsBitField.Flags.GuildVoiceStates,
    IntentsBitField.Flags.MessageContent,
  ],

  // Debug logs are disabled in silent mode
  silent: false,

  // Configuration for @SimpleCommand
  simpleCommand: {
    prefix: "!",
  },
});

async function updateDbGuilds() {
  const guilds = await bot.guilds.fetch();
  console.log(guilds);
  await Promise.all(
    guilds.map((g) =>
      prisma.guild.upsert({
        where: {
          id: g.id,
        },
        update: {
          name: g.name,
          iconUrl: g.iconURL(),
        },
        create: {
          id: g.id,
          name: g.name,
          iconUrl: g.iconURL(),
        },
      }),
    ),
  );
}

bot.on("guildCreate", async () => {
  await updateDbGuilds();
});

bot.once("ready", async () => {
  await updateDbGuilds();

  await bot.clearApplicationCommands(...bot.guilds.cache.map((g) => g.id));
  console.log("Commands cleared");

  await bot.initApplicationCommands();
  console.log("Commands initialized");

  await bot.user?.setPresence({
    activities: [
      {
        name: "commands",
        type: ActivityType.Listening,
      },
    ],
    status: "online",
  });

  console.log("Bot started");
});

bot.on("interactionCreate", (interaction: Interaction) => {
  const exceptionHandler = async (e: Error) => {
    console.error(`${e.name}: ${e.message}. Backtrace:`, e.stack);
    if (interaction.isRepliable()) {
      const errorMsg = `Error processing your command: ${e.message}`;
      if (interaction.replied) {
        // do nothing
      } else if (interaction.deferred) {
        await interaction.followUp(errorMsg);
      } else {
        await interaction.reply({ ephemeral: true, content: errorMsg });
      }
    }
  };
  process.once("uncaughtException", exceptionHandler);
  bot.executeInteraction(interaction);
});

bot.on("messageCreate", (message: Message) => {
  void bot.executeCommand(message);
});

async function run() {
  // The following syntax should be used in the commonjs environment
  //
  // await importx(__dirname + "/{events,commands}/**/*.{ts,js}");

  // The following syntax should be used in the ECMAScript environment
  await importx(`${dirname(import.meta.url)}/{events,commands}/**/*.{ts,js}`);

  // Let's start the bot
  if (!process.env.BOT_TOKEN) {
    throw Error("Could not find BOT_TOKEN in your environment");
  }

  // Log in with your bot token
  await bot.login(process.env.BOT_TOKEN);
}

void run();
