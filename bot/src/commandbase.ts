import { CommandInteraction } from "discord.js";

export class CommandBase {
  l = console;

  logInteraction(interaction: CommandInteraction) {
    this.l.log(
      JSON.stringify({
        username: interaction.member?.user.username,
        guild: interaction.guild?.name,
        // channel: interaction.channel?.name,
        command: interaction.command?.name,
        options: interaction.options.data,
      }),
    );
  }
}
