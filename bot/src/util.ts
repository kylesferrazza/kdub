export function need<T>(what: string, something: T | undefined | null): T {
  if (something === undefined) {
    throw new Error(`${what} was undefined`);
  }
  if (something === null) {
    throw new Error(`${what} was null`);
  }
  return something;
}
