{
  description = "kdub";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      devShell = pkgs.mkShell {
        name = "kdub";
        buildInputs = with pkgs; [
          nodejs
          openssl
          nodePackages.pnpm
          nodePackages.prisma
          vscode-langservers-extracted
        ];
        shellHook = with pkgs; ''
          export PATH="$PATH:$PWD/node_modules/.bin"
          export PATH="$PATH:$PWD/bot/node_modules/.bin"
          export PATH="$PATH:$PWD/site/node_modules/.bin"
          export PRISMA_QUERY_ENGINE_BINARY="${prisma-engines}/bin/query-engine"
          export PRISMA_QUERY_ENGINE_LIBRARY="${prisma-engines}/lib/libquery_engine.node"
          export PRISMA_INTROSPECTION_ENGINE_BINARY="${prisma-engines}/bin/introspection-engine"
          export PRISMA_FMT_BINARY="${prisma-engines}/bin/prisma-fmt"
          export DATABASE_URL="postgresql://postgres:example@localhost:5432/kdub?schema=public"
          export ENV="dev"
        '';
      };

      formatter = pkgs.alejandra;
    });
}
