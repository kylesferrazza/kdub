import { PrismaClient } from "@prisma/client";
const client = new PrismaClient();

async function seed() {
  await client.guild.createMany({
    data: [
      {
        id: "a",
        name: "200 Guild",
        iconUrl: "https://http.cat/200.jpg",
      },
      {
        id: "b",
        name: "201 Guild",
        iconUrl: "https://http.cat/201.jpg",
      },
    ],
  });
}

seed();
