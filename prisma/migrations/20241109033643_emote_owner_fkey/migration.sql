-- AlterTable
ALTER TABLE "Emote" ADD COLUMN     "ownerProvider" TEXT;

-- AddForeignKey
ALTER TABLE "Emote" ADD CONSTRAINT "Emote_ownerId_ownerProvider_fkey" FOREIGN KEY ("ownerId", "ownerProvider") REFERENCES "Account"("provider", "providerAccountId") ON DELETE SET NULL ON UPDATE CASCADE;
