-- AlterTable
ALTER TABLE "Emote" ADD COLUMN     "guildId" TEXT;

-- AddForeignKey
ALTER TABLE "Emote" ADD CONSTRAINT "Emote_guildId_fkey" FOREIGN KEY ("guildId") REFERENCES "Guild"("id") ON DELETE CASCADE ON UPDATE CASCADE;
