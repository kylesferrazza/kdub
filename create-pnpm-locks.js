const YAML = require('yaml')
const fs = require('fs')

const lockContents = YAML.parse(fs.readFileSync("pnpm-lock.yaml").toString());

function template(deps, devDeps, packages) {
  return YAML.stringify({
    "lockfileVersion": "6.0",
    "settings": {
      "autoInstallPeers": true,
      "excludeLinksFromLockfile": false,
    },
    "dependencies": deps,
    "devDependencies": devDeps,
    "packages": packages,
  })
}

function createLock(subProj) {
  const subProjObj = lockContents.importers[subProj];
  const deps = subProjObj.dependencies;
  const devDeps = subProjObj.devDependencies;
  const lockFileContents = template(deps, devDeps, lockContents.packages);
  fs.writeFileSync(subProj+"/pnpm-lock.yaml", lockFileContents);
}

createLock('site');
createLock('bot');
