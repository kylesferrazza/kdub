import { NextAuthConfig } from "next-auth";
import Discord from "next-auth/providers/discord";

export default {
  providers: [
    Discord({
      authorization:
        "https://discord.com/api/oauth2/authorize?scope=identify+email+guilds",
    }),
  ],
} satisfies NextAuthConfig;
