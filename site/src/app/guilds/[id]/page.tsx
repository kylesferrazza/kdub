import prisma from "@/db";
import {
  ImageList,
  ImageListItem,
  ImageListItemBar,
  ListSubheader,
} from "@mui/material";

export default async function Guild({ params }: { params: { id: string } }) {
  const { id } = params;
  const guild = await prisma.guild.findUnique({
    where: { id },
  });
  if (guild == null) return "Error getting guild!";
  const emotes = await prisma.emote.findMany({
    where: { guildId: guild.id },
  });
  return (
    <>
      <ImageList>
        <ImageListItem key="Subheader" cols={2}>
          <ListSubheader component="div">Emotes</ListSubheader>
        </ImageListItem>
        {emotes.map((emote) => (
          <ImageListItem key={emote.id}>
            <img src={emote.url} alt={emote.name} />
            <ImageListItemBar title={emote.name} subtitle="@todo-owner" />
          </ImageListItem>
        ))}
      </ImageList>
    </>
  );
}
// <img src={guild.iconUrl} alt="" />
