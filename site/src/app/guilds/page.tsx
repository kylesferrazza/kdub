import prisma from "@/db";
import Link from "next/link";

export const dynamic = "force-dynamic";
export default async function Guilds() {
  const guilds = await prisma.guild.findMany();
  return (
    <>
      <div>Guilds</div>
      <ul>
        {guilds.map((guild) => (
          <li key={guild.id}>
            <Link href={`/guilds/${guild.id}`}>{guild.name}</Link>
          </li>
        ))}
      </ul>
    </>
  );
}
