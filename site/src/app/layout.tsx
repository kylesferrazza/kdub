import type { Metadata } from "next";
import Image from "next/image";

import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import "@/app/globals.css";

import { AppRouterCacheProvider } from "@mui/material-nextjs/v14-appRouter";
import { AppProvider } from "@toolpad/core/nextjs";
import { DashboardLayout } from "@toolpad/core/DashboardLayout";
import { PageContainer } from "@toolpad/core/PageContainer";
import HomeIcon from "@mui/icons-material/Home";
import GroupIcon from "@mui/icons-material/Group";
import { Branding, Navigation } from "@toolpad/core";
import prisma from "@/db";
import { unstable_noStore } from "next/cache";

export const metadata: Metadata = {
  title: "KDUB",
  description: "Kyle's Discord Utility Bot",
};

async function makeNavigation(): Promise<Navigation> {
  const guilds = await prisma.guild.findMany();
  const guildItems = guilds.map(({ name, id, iconUrl }) => ({
    segment: id,
    title: name,
    icon: <img src={iconUrl || ""} width={24} height={24} />,
  }));
  return [
    {
      segment: "",
      title: "Home",
      icon: <HomeIcon />,
    },
    {
      segment: "guilds",
      title: "Guilds",
      icon: <GroupIcon />,
      children: guildItems,
    },
  ];
}

const BRANDING: Branding = {
  title: "KDUB",
  logo: <Image src="/icon.jpg" width={40} height={40} alt="logo" />,
};

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  unstable_noStore();
  const navigation = await makeNavigation();
  return (
    <html lang="en">
      <head></head>
      <body>
        <AppRouterCacheProvider options={{ enableCssLayer: true }}>
          <AppProvider navigation={navigation} branding={BRANDING}>
            <DashboardLayout>
              <PageContainer>{children}</PageContainer>
            </DashboardLayout>
          </AppProvider>
        </AppRouterCacheProvider>
      </body>
    </html>
  );
}
