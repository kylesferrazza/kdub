import NextAuth from "next-auth";
import { PrismaAdapter } from "@auth/prisma-adapter";
import authConfig from "./auth.config";
import { type Prisma } from "@prisma/client";
import prisma from "@/db";

async function getGuildIDs(token: string): Promise<string[]> {
  const headers = {
    Authorization: `Bearer ${token}`,
    "Content-Type": "application/json",
  };
  const res = await fetch("https://discord.com/api/users/@me/guilds", {
    headers,
  });
  const obj: Array<{ id: string; [key: string]: string }> = await res.json();
  return obj.map(({ id }) => id);
}

export const { handlers, signIn, signOut, auth } = NextAuth({
  adapter: PrismaAdapter(prisma),
  events: {
    signIn: async (u) => {
      const id = u.user.id;
      if (!id) return;
      if (!u.account) return;
      if (!u.account.access_token) return;
      if (!u.profile) return;

      const guildIds = await getGuildIDs(u.account.access_token);

      const existingGuilds = await prisma.guild.findMany({
        where: {
          id: {
            in: guildIds,
          },
        },
      });

      const userUpdate: Prisma.UserUpdateInput = {
        guilds: {
          connect: existingGuilds.map(({ id }) => ({ id })),
        },
      };

      if (u.profile.email) userUpdate.email = u.profile.email;
      if (u.profile.image_url) userUpdate.image = u.profile.image_url;
      if (u.profile.global_name) userUpdate.name = u.profile.global_name;

      await prisma.user.update({
        where: { id },
        data: userUpdate,
      });
    },
  },
  ...authConfig,
});
